﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using RenderComponents;

public class GameHandler : MonoBehaviour
{

    public Material material;
    public Mesh mesh;
    public Camera main;

    private static GameHandler instance;

    public GameHandler()
    {
        instance = this;
    }

    public static GameHandler GetInstance()
    {
        return instance;
    }

    private EntityManager entityManager;

    private void Awake()
    {


        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        EntityArchetype entityArchetype = entityManager.CreateArchetype(
            typeof(Translation),
            typeof(Rotation),
            typeof(ScaleComponent),
            typeof(MatrixTransformComponent),
            typeof(SpriteSheetComponent),
            typeof(SpriteSheetSharedComponent),
            typeof(SpriteSheetAnimationData)
        );
        Entity entity;

        float randomScale;
        float randomX;
        float randomY;

        for (int i = 0; i < 32000; i++)
        {
            randomScale = (0.1f + Random.value) * 1f;
            randomX = Random.Range(-4f, 4f);
            randomY = Random.Range(-2f, 2f);

            entity = entityManager.CreateEntity(entityArchetype);

            entityManager.SetComponentData(entity, 
                new Translation { 
                    Value = new Unity.Mathematics.float3(randomX, randomY, Random.Range(0, 50) ) 
            });
            entityManager.SetComponentData(entity,
                new SpriteSheetComponent
                {
                    uvCoords = new Vector4(0.25f, 1f, 0f, 0f)
                }
            );
            entityManager.SetSharedComponentData(entity, new SpriteSheetSharedComponent
            {
                material = material
            });

            entityManager.SetComponentData(entity, new SpriteSheetAnimationData
            {
                frameOffset = 0,
                frameCurrent = 0,
                frameCount = 4,
                frameTimer = 0f,
                frameTimerMax = 0.25f
            }
            );
            entityManager.SetComponentData(entity, new Rotation
            {
                Value = Quaternion.identity
            });

            entityManager.SetComponentData(entity, new ScaleComponent
            {
                scale = new Vector2(randomScale, randomScale)
            });

        }
    }
}
