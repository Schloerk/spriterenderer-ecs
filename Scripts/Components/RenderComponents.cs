﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

namespace RenderComponents
{
    // Tags
    public struct RenderTagComponent : IComponentData {}
    
    // Components
    public struct ScaleComponent : IComponentData {
        public Vector2 scale;
    }

    public struct MatrixTransformComponent : IComponentData {
        public Matrix4x4 matrix;
    }
    public struct SpriteSheetComponent : IComponentData {
        public Vector4 uvCoords;
    }

    public struct SpriteSheetAnimationData : IComponentData {
        public int frameCurrent; // Where are we in the current Animation
        public int frameOffset; // Where do we start in the current Spritesheet
        public int frameCount; // Where do we end in the current Spritesheet
        public float frameTimer; // Current time
        public float frameTimerMax; // Duration of each Sprite
    }

    // Shared Components
    public struct SpriteSheetSharedComponent : ISharedComponentData, System.IEquatable<SpriteSheetSharedComponent> {
        public Material material;

        public bool Equals(SpriteSheetSharedComponent other) {
            return material == other.material;
        }

        public override int GetHashCode() {
            return 1 + 13 * material.GetHashCode();
        }
    }

}
