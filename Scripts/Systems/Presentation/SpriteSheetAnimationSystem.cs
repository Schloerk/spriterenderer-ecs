﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using RenderComponents;

[UpdateInGroup(typeof(PresentationSystemGroup))]
public class SpriteSheetAnimationSystem : SystemBase
{
    protected override void OnUpdate() {
        float deltaTime = Time.DeltaTime;

        Entities
        .WithName("SpriteSheetAnimationSystem")
        .ForEach( (ref SpriteSheetAnimationData spriteSheetAnimationData, ref SpriteSheetComponent spriteSheetComponent) => {
            spriteSheetAnimationData.frameTimer += deltaTime;
            if(spriteSheetAnimationData.frameTimer >= spriteSheetAnimationData.frameTimerMax) {
                spriteSheetAnimationData.frameCurrent = 
                spriteSheetAnimationData.frameOffset + ( (spriteSheetAnimationData.frameCurrent + 1) % spriteSheetAnimationData.frameCount );

                spriteSheetComponent.uvCoords.x = 1f / spriteSheetAnimationData.frameCount;
                spriteSheetComponent.uvCoords.y = 1f;
                spriteSheetComponent.uvCoords.z = spriteSheetComponent.uvCoords.x * spriteSheetAnimationData.frameCurrent;
                spriteSheetComponent.uvCoords.w = 0f;

                spriteSheetAnimationData.frameTimer = 0f;
            }

        })
        .WithBurst()
        .ScheduleParallel();
    }
}
