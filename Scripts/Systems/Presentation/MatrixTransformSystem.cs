﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using RenderComponents;

[UpdateInGroup(typeof(PresentationSystemGroup))]
[UpdateAfter(typeof(RenderTagSystem))]
public class MatrixTransformSystem : SystemBase
{
    protected override void OnUpdate() {
        this.Dependency.Complete();
        Entities
        .WithName("SpriteSheetAnimationSystem")
        .ForEach( (ref MatrixTransformComponent matrixTransformComponent, in Translation translation, in Rotation rotation, in ScaleComponent scaleComponent ) => {
            matrixTransformComponent.matrix = Matrix4x4.TRS( translation.Value, rotation.Value, new Vector3(scaleComponent.scale.x, scaleComponent.scale.y, 1));
        })
        .WithBurst()
        .ScheduleParallel();
    }
}
