﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Mathematics;
using RenderComponents;

[UpdateInGroup(typeof(PresentationSystemGroup))]
public class RenderTagSystem : SystemBase
{
    private BeginPresentationEntityCommandBufferSystem beginPresentationEntityCommandBufferSystem;
    private Camera mainCamera;

    protected override void OnCreate() {
        base.OnCreate();
        beginPresentationEntityCommandBufferSystem = World.GetOrCreateSystem<BeginPresentationEntityCommandBufferSystem>();
        mainCamera = GameHandler.GetInstance().main;
    }
    protected override void OnUpdate() {
        var ecb = beginPresentationEntityCommandBufferSystem.CreateCommandBuffer().ToConcurrent();

        float3 cameraPosition = mainCamera.transform.position;
        float xExtend = mainCamera.orthographicSize * Screen.width / Screen.height;

        float yMin = cameraPosition.y - mainCamera.orthographicSize;
        float yMax = cameraPosition.y + mainCamera.orthographicSize;
        float xMin = cameraPosition.x - xExtend;
        float xMax = cameraPosition.x + xExtend;

        Entities
        .WithName("Add_RenderTagComponent")
        .WithNone<RenderTagComponent>()
        .ForEach( (Entity entity, int entityInQueryIndex, in Translation translationComponent) => {
            float3 position = translationComponent.Value;
            if (position.y > yMin && position.y < yMax && position.x > xMin && position.x < xMax) {
                ecb.AddComponent<RenderTagComponent>(entityInQueryIndex, entity, new RenderTagComponent());
            }
        })
        .WithBurst()
        .ScheduleParallel();

         
        Entities
        .WithName("Remove_RenderTagComponent")
        .WithAll<RenderTagComponent>()
        .ForEach( (Entity entity, int entityInQueryIndex, in Translation translationComponent) => {
            float3 position = translationComponent.Value;
            if (position.y < yMin || position.y > yMax || position.x < xMin || position.x > xMax) {
                ecb.RemoveComponent<RenderTagComponent>(entityInQueryIndex, entity);
            }
        })
        .WithBurst()
        .ScheduleParallel(); 

        this.Dependency.Complete();

    }
}
