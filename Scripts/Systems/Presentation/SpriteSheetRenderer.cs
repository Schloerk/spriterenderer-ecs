﻿using System;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Scripting;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using RenderComponents;

[UpdateInGroup(typeof(PresentationSystemGroup))]
[UpdateAfter(typeof(SpriteSheetAnimationSystem))]
[UpdateAfter(typeof(MatrixTransformSystem))]
public class SpriteSheetRenderer : SystemBase
{
    private static readonly string MAIN_TEX_UV = "_MainTex_UV";
    private static readonly int BATCH_SIZE = 1023;

    [BurstCompile]
    private struct QuickSortDepthJob : IJob
    {
        public NativeArray<EntityData> entityDataArray;

        [ReadOnly]
        public int min;
        [ReadOnly]
        public int max;
        public void Execute()
        {
            quicksort(entityDataArray, min, max);
        }

        private void quicksort(NativeArray<EntityData> liste, int li, int re)
        {
            int i = li, j = re;
            EntityData tmp;
            EntityData pivot = liste[(li + re) / 2];
            while (i <= j)
            {
                while (liste[i].position.z > pivot.position.z)
                    i++;
                while (liste[j].position.z < pivot.position.z)
                    j = j - 1;
                if (i <= j)
                {
                    tmp = liste[i];
                    liste[i] = liste[j];
                    liste[j] = tmp;
                    i++;
                    j = j - 1;
                }
            };
            if (li < j)
            {
                quicksort(liste, li, j);
            }
            if (i < re)
            {
                quicksort(liste, i, re);
            }
        }

    }

    [BurstCompile]
    private struct FillOrderMapJob : IJob
    {
        [ReadOnly]
        public NativeArray<EntityData> entityDataArray;
        public NativeList<int> orderList;
        public void Execute()
        {
            EntityData lastEntityData = entityDataArray[0];
            EntityData currentEntityData;

            for (int i = 1; i < entityDataArray.Length; i++)
            {
                currentEntityData = entityDataArray[i];
                if(currentEntityData.sharedComponentIndex != lastEntityData.sharedComponentIndex) {
                    orderList.Add(i - 1);
                }
                lastEntityData = currentEntityData;
            }
            
            orderList.Add(entityDataArray.Length - 1);
        }
    }

    [BurstCompile]
    private struct ExtractMatrixArrayJob : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<EntityData> entityDataArray;
        [WriteOnly]
        public NativeArray<Matrix4x4> matrixArray;
        public void Execute(int i)
        {
            matrixArray[i] =  entityDataArray[i].matrix;    
        }
    }

    [BurstCompile]
    private struct ExtractUVArrayJob : IJobParallelFor
    {
        [ReadOnly]
        public NativeArray<EntityData> entityDataArray;
        [WriteOnly]
        public NativeArray<Vector4> uvArray;
        public void Execute(int i)
        {
            uvArray[i] =  entityDataArray[i].uv;    
        }
    }

    private EntityQuery entityQuery;
    private ArchetypeChunkComponentType<Translation> translationType;
    private ArchetypeChunkComponentType<MatrixTransformComponent> matrixType;
    private ArchetypeChunkComponentType<SpriteSheetComponent> spriteSheetType;
    private ArchetypeChunkSharedComponentType<SpriteSheetSharedComponent> spriteSheetSharedType;

    private struct EntityData
    {
        public Vector3 position;
        public Matrix4x4 matrix;
        public Vector4 uv;
        public int sharedComponentIndex;
    }

    protected override void OnCreate()
    {
        drawMesh = GameHandler.GetInstance().mesh;
        shaderMainTextId = Shader.PropertyToID(MAIN_TEX_UV);
        materialPropertyBlock = new MaterialPropertyBlock();

        entityQuery = GetEntityQuery(
            ComponentType.ReadOnly<Translation>(),
            ComponentType.ReadOnly<MatrixTransformComponent>(),
            ComponentType.ReadOnly<SpriteSheetComponent>(),
            ComponentType.ReadOnly<RenderTagComponent>()
        );
    }

 [BurstCompile]
    private struct FillEntityListJob : IJob
    {
        [ReadOnly]
        public ArchetypeChunkComponentType<Translation> translationType;
        [ReadOnly]
        public ArchetypeChunkComponentType<MatrixTransformComponent> matrixType;
        [ReadOnly]
        public ArchetypeChunkComponentType<SpriteSheetComponent> spriteSheetType;
        [ReadOnly]
        public ArchetypeChunkSharedComponentType<SpriteSheetSharedComponent> spriteSheetSharedType;
        [ReadOnly]
        public ArchetypeChunk currentChunk;

        public NativeList<EntityData> entityDataList;

        public void Execute()
        {
            NativeArray<Translation> translationArray = currentChunk.GetNativeArray(translationType);
            NativeArray<MatrixTransformComponent> matricesArray = currentChunk.GetNativeArray(matrixType);
            NativeArray<SpriteSheetComponent> spritesArray = currentChunk.GetNativeArray(spriteSheetType);
            int sharedComponentIndex = currentChunk.GetSharedComponentIndex(spriteSheetSharedType);

            EntityData currentEntityData;
            for (int j = 0; j < translationArray.Length; j++)
            {
                    currentEntityData = new EntityData();
                    currentEntityData.position = translationArray[j].Value;
                    currentEntityData.sharedComponentIndex = sharedComponentIndex;
                    currentEntityData.matrix = matricesArray[j].matrix;
                    currentEntityData.uv = spritesArray[j].uvCoords;
                    entityDataList.Add(currentEntityData);

            }
        }
    }


    private int shaderMainTextId;
    private Mesh drawMesh;
    private MaterialPropertyBlock materialPropertyBlock;
    private ArchetypeChunk currentChunk;
    private NativeArray<Translation> translationArray;
    private NativeArray<MatrixTransformComponent> matricesArray;
    private NativeArray<SpriteSheetComponent> spritesArray;
    private int sharedComponentIndex;
    private NativeArray<ArchetypeChunk> archetypeChunks;
    private NativeList<EntityData> entityDataList;
    private NativeArray<EntityData> entityDataArray;
    private QuickSortDepthJob sortDepthJob;
    private int minBatches;
    private NativeList<int> orderList;
    private int currentSharedComponentIndex;
    private SpriteSheetSharedComponent currentSharedComponent;
    private NativeArray<Matrix4x4> matrixArray;
    private NativeArray<Vector4> uvArray;
    private NativeArray<Matrix4x4> batchMatrixArray = new NativeArray<Matrix4x4>(BATCH_SIZE, Allocator.Persistent);
    private NativeArray<Vector4> batchUVArray = new NativeArray<Vector4>(BATCH_SIZE, Allocator.Persistent);
    private int upperBound;
    private int lowerBound;

    protected override void OnUpdate()
    {
        this.Dependency.Complete();

        translationType = EntityManager.GetArchetypeChunkComponentType<Translation>(true);
        matrixType = EntityManager.GetArchetypeChunkComponentType<MatrixTransformComponent>(true);
        spriteSheetType = EntityManager.GetArchetypeChunkComponentType<SpriteSheetComponent>(true);
        spriteSheetSharedType = GetArchetypeChunkSharedComponentType<SpriteSheetSharedComponent>();
        
#if UNITY_EDITOR
        Profiler.BeginSample("FillEntityList");
#endif

        archetypeChunks = entityQuery.CreateArchetypeChunkArray(Allocator.TempJob);
        entityDataList = new NativeList<EntityData>(Allocator.TempJob);

        // Parallelifying this decreases performance
        for (int i = 0; i < archetypeChunks.Length; i++)
        {
            currentChunk = archetypeChunks[i];
            new FillEntityListJob()
            {
                translationType = translationType,
                matrixType = matrixType,
                spriteSheetType = spriteSheetType,
                spriteSheetSharedType = spriteSheetSharedType,
                currentChunk = currentChunk,
                entityDataList = entityDataList
            }
            .Run();
        }

        archetypeChunks.Dispose();
        entityDataArray = entityDataList.AsArray();

#if UNITY_EDITOR
        Profiler.EndSample();
        Profiler.BeginSample("SortJob");
#endif
        sortDepthJob = new QuickSortDepthJob()
        {
            entityDataArray = entityDataArray,
            min = 0,
            max = entityDataArray.Length - 1
        };
        sortDepthJob.Schedule().Complete();

#if UNITY_EDITOR
        Profiler.EndSample();
        Profiler.BeginSample("FillOrderList");
#endif

        orderList = new NativeList<int>(10, Allocator.TempJob);
        new FillOrderMapJob()
        {
            entityDataArray = entityDataArray,
            orderList = orderList
        }.Run();

#if UNITY_EDITOR
        Profiler.EndSample();
        Profiler.BeginSample("ExtractLists");
#endif
            matrixArray = new NativeArray<Matrix4x4>(entityDataArray.Length, Allocator.TempJob);
            uvArray = new NativeArray<Vector4>(entityDataArray.Length, Allocator.TempJob);

            JobHandle matriceJob =
            new ExtractMatrixArrayJob()
            {
               entityDataArray = entityDataArray,
               matrixArray = matrixArray
            }.Schedule(entityDataArray.Length, 64);

            JobHandle uvJob =
            new ExtractUVArrayJob()
            {
                entityDataArray = entityDataArray,
                uvArray = uvArray
            }.Schedule(entityDataArray.Length, 64);
            JobHandle.CompleteAll(ref matriceJob, ref uvJob);

#if UNITY_EDITOR
        Profiler.EndSample();
        Profiler.BeginSample("Render");
#endif
        int currentOrderIndex;

        for (int i = 0; i < orderList.Length; i++)
        {
            currentOrderIndex = orderList[i];
            currentSharedComponentIndex = entityDataArray[i].sharedComponentIndex;
            currentSharedComponent = EntityManager.GetSharedComponentData<SpriteSheetSharedComponent>(currentSharedComponentIndex);

            for (int j = 0; j < currentOrderIndex; j += BATCH_SIZE)
            {

                upperBound = Unity.Mathematics.math.min(currentOrderIndex + 1 - j, BATCH_SIZE);
                lowerBound = Unity.Mathematics.math.min(j, currentOrderIndex - 1);

                NativeArray<Matrix4x4>.Copy(matrixArray, lowerBound, batchMatrixArray, 0, upperBound);
                NativeArray<Vector4>.Copy(uvArray, lowerBound, batchUVArray, 0, upperBound);

                materialPropertyBlock.SetVectorArray(shaderMainTextId, batchUVArray.ToArray());

                Graphics.DrawMeshInstanced(
                drawMesh,
                0,
                currentSharedComponent.material,
                batchMatrixArray.ToArray(),
                upperBound,
                materialPropertyBlock
                );
            }
        }
#if UNITY_EDITOR
        Profiler.EndSample();
#endif

        entityDataList.Dispose();
        orderList.Dispose();
        uvArray.Dispose();
        matrixArray.Dispose();

    }
    protected override void OnDestroy()
    {
        batchMatrixArray.Dispose();
        batchUVArray.Dispose();
    }
}
